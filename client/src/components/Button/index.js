import React from "react";

//Ancienne manière de faire

class ButtonOld extends React.PureComponent {
    render(){
        return <button onClick={(alert('test')}>Test</button>;
    }
}

//Nouvelle manière de faire
function Button() {
    const too = () => alert('test');
    return <button onClick={too}>Test</button>;
}

export default Button;